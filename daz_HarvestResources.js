/*:
 * @plugindesc Daz Harvest Resources
 * @author Mike Dazzo
 *
 * @help Allows meta tagging of an even to treat is as a resource.
 * 
 * Requirements:
 * LeNotifications.js - https://forums.rpgmakerweb.com/index.php?threads/notifications-system-1-3-add-on.47231/
 * LeUtilities.js - https://drive.google.com/file/d/0B_97KWiBTWbbMEFmNkM4aG5OTVU/view?usp=sharing
 *  
 * Examples:
 * Basic:
 * <resource:wood><resourceCount:6><itemId:10>
 *
* @param testParam1
 * @type note
 * @desc Some Param
 * @default 10
 * */

var Global_ResourceTracker = {};

(function() {
var alias_GameEventInit = Game_Event.prototype.initialize;

Game_Event.prototype.initialize = function(mapId, eventId) 
{
    alias_GameEventInit.call(this, mapId, eventId);
    this._mapId = mapId;
    this._eventId = eventId;
    try
    {
        this._isResource = this.IsResource();
        if (this._isResource)
        {
            var thisEvent = $dataMap.events[this._eventId];
            this._resourceType = thisEvent.meta.resource;
            if (Global_ResourceTracker[this._mapId + "." + this._eventId] != null)
            {
                this._resourceCount = Global_ResourceTracker[this._mapId + "." + this._eventId].count;
            }
            else
            {
                this._resourceCount = parseInt(thisEvent.meta.resourceCount);
                this._itemId = parseInt(thisEvent.meta.itemId);
                var oResourceItem = {};
                oResourceItem.count = this._resourceCount;
                oResourceItem.type = this._resourceType;
                oResourceItem.itemId = this._itemId;
                oResourceItem.originalCount = this._resourceCount;
                Global_ResourceTracker[this._mapId + "." + this._eventId] = oResourceItem;
            }
            //Overwrite What it does?
            if (this._resourceCount <= 0)
            {
                $gameMap.eraseEvent(this._eventId);
            }
        }
    }
    catch (er)
    {
        this._isResource = false
        this._resourceCount = 0;
        this._itemId = null;
        this._resourceType = null;
    }
    this.locate(this.event().x, this.event().y);
    this.refresh();
};

var alias_GameEventstart = Game_Event.prototype.start;
Game_Event.prototype.start = function() {
    alias_GameEventstart.call(this);
    if (this.IsResource())
    {
        this.HarvestResource();
    }
};

Game_Event.prototype.IsResource = function()
{
    return $dataMap.events[this._eventId].meta.resource != undefined;
}

Game_Event.prototype.HarvestResource = function()
{
    var thisEvent = $dataMap.events[this._eventId];
    if (Global_ResourceTracker[this._mapId + "." + this._eventId] != null)
    {
        var iOriginalCount = Global_ResourceTracker[this._mapId + "." + this._eventId].count;
        var sType = Global_ResourceTracker[this._mapId + "." + this._eventId].type;
        var iItemId = Global_ResourceTracker[this._mapId + "." + this._eventId].itemId;
        if (iOriginalCount > 0)
        {
            var iHarvestCount = Math.floor(Math.random() * 3 + 1); //todo: Should change this with different level tools?
            if (iHarvestCount > iOriginalCount)
            {
                iHarvestCount = iOriginalCount;
            }
            Global_ResourceTracker[this._mapId + "." + this._eventId].count -= iHarvestCount;
            if (Global_ResourceTracker[this._mapId + "." + this._eventId].count <= 0)
            {
                //Items gone.  Start countdown
                var oRegrowAt = Date.now();
                oRegrowAt += (1000 * 60 * 1);   //todo: pull regrowth rate from meta

                Global_ResourceTracker[this._mapId + "." + this._eventId].regrowthDateTime = oRegrowAt;
                $gameMap.eraseEvent(this._eventId);
            }
            //todo Show Animation?
            

            $gameParty.gainItem($dataItems[iItemId], iHarvestCount);
            LeNotifManager.add("Gained " + iHarvestCount + " " + sType);
        }
        else
        {
            if (Global_ResourceTracker[this._mapId + "." + this._eventId].regrowthDateTime < Date.now())
            {
                //Refill Items
                Global_ResourceTracker[this._mapId + "." + this._eventId].count = Global_ResourceTracker[oMapID + "." + oEventID].originalCount;
                //Call self to harvest
                this.HarvestResource(this._mapId, this._eventId);
            }
            else
            {
                LeNotifManager.add("Nothing new to harvest.");
            }
        }
    }
}

Game_Event.prototype.update = function() {
    Game_Character.prototype.update.call(this);
    this.checkEventTriggerAuto();
    this.updateParallel();

    //This keeps the item out of the page while it has no resources.
    if (this._resourceCount <= 0 && this._removed == null)
    {
        $gameMap.eraseEvent(this._eventId);
        this._removed = true;
    }
};
})();